from gemmail_python import gembox


class TestGemBoxParse:
    def test_parse1(self, gembox1):
        box = gembox.parseGemBox(None, gembox1)
        assert len(box.Mails) == 1

        mail1, idx = box.getGemMailWithID('lhasf324h3gk3j3guh3hkh3uiuh33kh3u')

        assert mail1.containsSender('misfin@auragem.letz.dev')
        assert mail1.containsSender('clseibold@hashnix.club')

        assert len(mail1.Timestamps) == 2

        for tag in ['Inbox', 'Unread', 'Inbox.sub_folder']:
            assert mail1.hasTag(tag)
    
    def test_parse2(self, gembox2):
        box = gembox.parseGemBox(None, gembox2)
        assert len(box.Mails) == 2

        mail1, idx = box.getGemMailWithID('lhasf324h3gk3j3guh3hkh3uiuh33kh3u')

        assert mail1.containsSender('misfin@auragem.letz.dev')
        assert mail1.containsSender('clseibold@hashnix.club')

        assert len(mail1.Timestamps) == 2

        for tag in ['Inbox', 'Unread', 'Inbox.sub_folder']:
            assert mail1.hasTag(tag)
