from pathlib import Path
import os.path
import pytest


samples_p = Path(os.path.dirname(__file__)).joinpath('samples')


@pytest.fixture
def gembox1():
    return samples_p.joinpath('gembox').joinpath('box1.gemboxf').read_text()


@pytest.fixture
def gembox2():
    return samples_p.joinpath('gembox').joinpath('box2.gemboxf').read_text()
